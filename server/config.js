const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPathArtist: path.join(rootPath, 'public/uploads/artist'),
  uploadPathAlbum: path.join(rootPath, 'public/uploads/album'),
  uploadPathAvatar: path.join(rootPath, 'public/uploads/avatar'),
  dbUrl: 'mongodb://localhost/melomanFM',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '290704098475827',
    appSecret: '75c1f2aab357b0246a02ef44b3f87da2'
  }
};
