const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Artist = require('../models/Artist');
const config = require("../config");
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathArtist);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
  try {
    let criteria = {public: true};

      if (req.user) {
        criteria = {
          $or: [
            {public: true},
            {user: req.user._id}
          ]
        }
      }
      if (req.user && req.user.role === 'admin') {
        criteria = {
          $or: [
            {public: true},
            {public: false},
            {user: req.user._id}
          ]
        }
      }

    let artist = await Artist.find(criteria);

    return res.send(artist);
  } catch(e) {
    if (e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(console.log(e));
  }
});

router.post('/', [auth, permit('admin', 'user'), upload.single('photo')], (req, res) => {
  const artistInfo = {...req.body, user: req.user._id};

  if (req.file) {
    artistInfo.photo = req.file.filename;
  }

  const artist = new Artist(artistInfo);

  artist.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error))
});

router.post('/:id/publish', [auth, permit('admin')], async (req, res) => {
  const artist = await Artist.findById(req.params.id);

  if(!artist) {
    return res.sendStatus(404);
  }

  artist.public = !artist.public;

  await artist.save();

  return res.send(artist);
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
  Artist.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(error => res.status(403).send(error))
});

module.exports = router;