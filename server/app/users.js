const express = require('express');
const config = require("../config");
const multer = require('multer');
const path = require('path');
const axios = require('axios');
const nanoid = require('nanoid');

const User = require('../models/User');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathAvatar);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.post('/', upload.single('avatar'), async (req, res) => {
  const user = new User({
    username: req.body.username,
    displayName: req.body.displayName,
    password: req.body.password,
    avatar: req.body.avatar
  });

  if (req.file) {
    user.avatar = req.file.filename;
    user.avatar = `http://localhost:8000/uploads/avatar/${user.avatar}`;
  }

  user.generateToken();

  try {
    await user.save();
    return res.send({message: 'User registered', user});
  } catch (error) {
    return res.status(400).send(error)
  }
});

router.post('/sessions', async (req, res) => {
  const user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(400).send({error: 'User does not exist'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(400).send({error: 'Password incorrect'});
  }

  user.generateToken();

  await user.save();

  res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Logged out'};

  if(!token) {
    return res.send(success);
  }
  const user = await User.findOne({token});

  if(!user) {
    return res.send(success);
  }

  user.generateToken();
  await user.save();

  return res.send(success);
});

router.post('/facebookLogin', async (req, res) => {
  const inputToken = req.body.accessToken;
  const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;
  const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

  try {
    const response = await axios.get(debugTokenUrl);
    if (response.data.data.error) {
      return res.status(401).send({message: 'Facebook token incorrect'});
    }

    if (response.data.data.user_id !== req.body.id) {
      return res.status(401).send({message: 'Wrong user ID'});
    }

    let user = await User.findOne({facebookId: req.body.id});

    if (!user) {
      user = new User ({
        username: req.body.email || req.body.id,
        displayName: req.body.name,
        avatar: req.body.picture.data.url,
        password: nanoid(),
        facebookId: req.body.id,
      });
    }

    user.generateToken();

    await user.save();

    return res.send({message: 'Login registered', user});

  } catch (error){
    console.log(error);
    return res.status(418).send({message: 'Facebook token incorrect'});
  }
});


module.exports = router;
