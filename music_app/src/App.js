import React, {Component, Fragment} from 'react';
import {Container} from "reactstrap";
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {NotificationContainer} from 'react-notifications';

import Toolbar from "./components/UI/Toolbar/Toolbar";

import './App.css';
import {logoutUser} from "./store/actions/usersActions";
import Routes from "./Routes";

class App extends Component {
  render() {
    return (
      <Fragment>
        <header>
          <NotificationContainer/>
          <Toolbar
            user={this.props.user}
            logout={this.props.logoutUsers}
          />
        </header>
        <Container style={{marginTop: '20px'}}>
          <Routes user={this.props.user}/>
        </Container>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  logoutUsers: () => dispatch(logoutUser())
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
