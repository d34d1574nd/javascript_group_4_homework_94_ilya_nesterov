import React from 'react';
import {Route, Switch} from "react-router-dom";

import ArtistPage from "./containers/ArtsitsPage/ArtistPage";
import ArtistForm from "./components/ArtistForm/ArtistForm";
import AlbumForm from "./components/AlbumForm/AlbumForm";
import TrackForm from "./components/TrackForm/TrackForm";
import Registration from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AlbumPage from "./containers/AlbumPage/AlbumPage";
import TrackPage from "./containers/TrackPage/TrackPage";
import TrackHistory from "./containers/TrackHistory/TrackHistory";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={ArtistPage}/>
      <Route path="/add_artist" exact component={ArtistForm}/>
      <Route path="/add_album" exact component={AlbumForm}/>
      <Route path="/add_track" exact component={TrackForm}/>
      <Route path="/register" exact component={Registration}/>
      <Route path="/login" exact component={Login}/>
      <Route path="/albums/:id/:artist" exact component={AlbumPage}/>
      <Route path="/tracks/:id/:album/:artist" exact component={TrackPage}/>
      <Route path="/track_history" exact component={TrackHistory}/>
    </Switch>
  );
};

export default Routes;