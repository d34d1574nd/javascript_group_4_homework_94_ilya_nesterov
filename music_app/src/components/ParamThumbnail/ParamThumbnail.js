import React from 'react';

import {apiURL} from "../../constants";

const style = {
    width: '300px',
};

const ParamThumbnail = props => {
    let image = null;

    if (props.image) {
        image = apiURL + '/uploads/' + props.param + '/' + props.image;
    }

    return <img src={image} style={style} className="img-thumbnail" alt="Artist" />
};

export default ParamThumbnail;