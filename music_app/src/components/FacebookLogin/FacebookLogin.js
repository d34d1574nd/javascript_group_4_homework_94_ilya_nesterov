import React, {Component} from 'react';
import {connect} from "react-redux";
import FacebookLoginButton from 'react-facebook-login/dist/facebook-login-render-props';
import {Button} from "reactstrap";
import {NotificationManager} from "react-notifications";
import {facebookLogin} from "../../store/actions/usersActions";

class FacebookLogin extends Component {

  facebookLogin = data => {
    if (data.error) {
      NotificationManager.error('Wrong');
    } else if (!data.name) {
      NotificationManager.warning('You pressed cancel');
    } else {
      this.props.facebookLogin(data);
    }
  };

  render() {
    return (
      <FacebookLoginButton
        appId="290704098475827"
        fields="name,email,picture"
        callback={this.facebookLogin}
        render={renderProps => (
          <Button onClick={renderProps.onClick} color='primary' outline>Login with facebook</Button>
        )}
      />
    );
  }
}

const mapDispatchToProps = dispatch => ({
  facebookLogin: userData => dispatch(facebookLogin(userData))
});

export default connect(null, mapDispatchToProps)(FacebookLogin);