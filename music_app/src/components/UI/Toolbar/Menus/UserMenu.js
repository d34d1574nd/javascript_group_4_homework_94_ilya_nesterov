import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink} from "react-router-dom";
import avatar from '../../../../assets/images/avatar.jpeg';

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello,
      <img src={!user.avatar && user.avatar === '' ? avatar : user.avatar} className='userAvatar' alt='user'/>
      {user.role === 'admin' ? <strong>admin </strong> : null} {user.displayName}
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem>
        <NavLink to='/add_artist'>Add artist</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/add_album'>Add album</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/add_track'>Add track</NavLink>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem>
        <NavLink to='/track_history'>Track history</NavLink>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem onClick={logout}>
        Log out
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default UserMenu;