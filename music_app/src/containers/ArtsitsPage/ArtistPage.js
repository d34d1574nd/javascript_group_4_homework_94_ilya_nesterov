import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteArtist, fetchArtist, postArtistPublic} from "../../store/actions/artistActions";
import {Button, ButtonGroup, Card, CardBody, CardFooter, CardText, CardTitle, Col, Row} from "reactstrap";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {NavLink} from "react-router-dom";
import Spinner from "../../components/UI/Spinner/Spinner";


class ArtistPage extends Component {

  componentDidMount() {
    this.props.getArtist()
  }

  render() {
    if (this.props.loading) {
      return <Spinner />;
    }


    const artist = this.props.artist.map(artist => (
          <Col sm="3" key={artist._id} className='blockArtist'>
            <Card outline color={artist.public === true ? "primary" : "danger"} >
              <div className="image">
                <ParamThumbnail
                  param='artist'
                  image={artist.photo}
                />
              </div>

              <CardBody>
                <strong>Artist: </strong>
                <CardTitle>{artist.name}</CardTitle>
                <strong>Information: </strong>
                <CardText>{artist.information}</CardText>
              </CardBody>
              {
                artist.public === true
                  ?
                  <CardFooter className="text-muted">
                    <NavLink to={`/albums/${artist._id}/${artist.name}`}>Albums</NavLink>
                    {this.props.user && this.props.user.role === 'admin' ?
                      <Button
                        style={{borderRadius: '50%', padding: '0 7px', float: 'right'}}
                        onClick={() => this.props.deleteArtist(artist._id)} outline color="danger">
                        <i className="fas fa-times" />
                      </Button>
                      : null}
                  </CardFooter>
                  :
                  <CardFooter className="text-muted">
                    <span>Not published</span>
                    {this.props.user && this.props.user.role === 'admin' ?
                      <ButtonGroup style={{float: 'right'}}>
                        <Button style={{padding: '0 7px'}} onClick={() => this.props.postArtistPublic(artist._id)} outline color="success">Public</Button>
                        <Button style={{padding: '0 7px'}} onClick={() => this.props.deleteArtist(artist._id)} outline color="danger">
                          <i className="fas fa-times" />
                        </Button>
                      </ButtonGroup>
                      : null}
                  </CardFooter>
              }
            </Card>
          </Col>
        )
    );
    return (
      <Row>
        {artist}
      </Row>
    );
  }
}

const mapStateToProps = state => ({
  artist: state.artist.artist,
  loading: state.artist.loading,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getArtist: () => dispatch(fetchArtist()),
  postArtistPublic: artistId => dispatch(postArtistPublic(artistId)),
  deleteArtist: artistId => dispatch(deleteArtist(artistId))
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistPage);