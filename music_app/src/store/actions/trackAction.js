import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";
import {push} from 'connected-react-router';

export const FECTH_TRACK_SUCCESS = 'FECTH_TRACK_SUCCESS';

const fetchAlbumsSuccess = tracks => ({type: FECTH_TRACK_SUCCESS, tracks});

export const fetchTrack = idAlbum => {
  return dispatch => {
    return axios.get('/tracks?album=' + idAlbum).then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    );
  };
};

export const postTrack = track => {
  return dispatch => {
    return axios.post('/tracks', track).then(
      response => {
        NotificationManager.info('Track created, but not public!');
        dispatch(fetchTrack(response.data.album._id));
        dispatch(push(`/tracks/${response.data.album._id}/${response.data.album.titleAlbum}/${response.data.album.artist.name}`))
      }
    )
  };
};

export const postTrackPublic = trackData => {
  return dispatch => {
    const trackId = trackData._id;
    return axios.post(`/tracks/${trackId}/public`).then(
      () => {
        dispatch(fetchTrack(trackData.album._id));
        NotificationManager.success('Track published!');
      }
    )
  };
};

export const deleteTrack = trackData => {
  return dispatch => {
    const trackId = trackData._id;
    return axios.delete(`/tracks/${trackId}`).then(
      () => {
        dispatch(fetchTrack(trackData.album._id));
        NotificationManager.error('Track deleted!');
      }
    )
  };
};