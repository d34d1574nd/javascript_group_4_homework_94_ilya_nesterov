import React from 'react';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import thunkMiddleware from 'redux-thunk';
import {createBrowserHistory} from "history";
import {connectRouter, routerMiddleware, ConnectedRouter} from "connected-react-router";
import * as serviceWorker from './serviceWorker';
import axios from './axios-api';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import App from './App';
import artistReducer from "./store/reducers/artistReducer";
import albumReducer from "./store/reducers/albumReducer";
import trackReducer from "./store/reducers/trackReducer";
import usersReducer from "./store/reducers/usersReducer";
import TrackHistoryReducer from "./store/reducers/trackHistoryReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./store/localStorage";


const history = createBrowserHistory();

const rootReducer = combineReducers({
  router: connectRouter(history),
  artist: artistReducer,
  album: albumReducer,
  track: trackReducer,
  users: usersReducer,
  history: TrackHistoryReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadFromLocalStorage();

const store = createStore(rootReducer, persistedState, enhancers);

store.subscribe(() => {
  saveToLocalStorage({
    users: {
      user: store.getState().users.user
    }
  })
});

axios.interceptors.request.use(config => {
  try {
    config.headers['Authorization'] = store.getState().users.user.token;
  } catch(e) {
    // do nothing, user is not logged in
  }

  return config
});

const app = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App/>
    </ConnectedRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
serviceWorker.unregister();
